import org.bluej.WeatherStation.Units.Temperature;
import org.bluej.WeatherStation.Implementations.Factory;
import org.bluej.WeatherStation.Implementations.Platform;
import org.bluej.WeatherStation.Sensors.TemperatureSensor;

import org.bluej.WeatherStation.Implementations.PiAmbientTemperatureSensor;
import org.bluej.WeatherStation.Drivers.HTU21D;
import javafx.application.Application;
import javafx.embed.swing.JFXPanel;

/**
 * Write a description of class SoilTemperature here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class AmbientTemperature
{
   /**
     * How often to check the Temperature.
     */
    public static final int INTERVAL_MILLIS = 10000;

    /**
     * How many times to check the Temperature when asked for a reading.
     */
    public static final int ITERATIONS = 4; // instance variables - replace the example below with your own
    private TemperatureSensor sensor;

    /**
     * Constructor for objects of class SoilTemperature
     */
    public AmbientTemperature()
    {
        // initialise instance variables
       
             sensor = new Factory(Platform.WEATHERSTATION_V1).getAmbientTemperatureSensor();
            //Platform.WEATHERSTATION_V1
        

    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public void plot()
    {
        // put your code here
        int[] x=new int[3];
        x[0]=80;
        x[1]=20;
        x[2]=30;
        int[] y=new int[3];
        y[0]=10;
        y[1]=20;
        y[2]=30;
        javafx.application.Platform.setImplicitExit(false);
        //  SwingUtilities.invokeLater(() -> {
        new JFXPanel(); // initializes JavaFX environment

        // });

        // Platform.runLater(() -> {

        LineChartSample graph=new LineChartSample();
        graph.x=x;
        graph.y=y;
        graph.launch1();

    }
    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public Temperature[] readTemperature(int y)
    {
        Temperature[] temperatures = new Temperature[ITERATIONS];
try{
        for (int i = 0; i < ITERATIONS; i++) {
            Thread.sleep(INTERVAL_MILLIS);
            temperatures[i] = sensor.getTemperature();
            System.out.println(temperatures[i].inCelsius());
        }
    }
         catch (Exception e){
            System.out.println("Driver DS18B20 not found");

        }  
           return temperatures; 
    }
}
