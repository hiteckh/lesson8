import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
 
 
public class LineChartSample extends Application {
 private   static   XYChart.Series series=new XYChart.Series();
  private static  NumberAxis xAxis;
 private static NumberAxis yAxis;
 private static   LineChart<Number,Number> lineChart;
private static Stage primarystage;
private static Scene scene; 
public static int[] x=new int[3];
public static int[] y=new int[3];
public void launch1()
{launch();
}
    @Override public void start(Stage stage) {
        stage.setTitle("Figure 1");
        //defining the axes
        primarystage=stage;
        xAxis = new NumberAxis();
        yAxis = new NumberAxis();
        lineChart=new LineChart<Number,Number>(xAxis,yAxis);
        xAxis.setLabel("Time in Seconds");
        //creating the chart
        

                
        lineChart.setTitle("Soli Temperature");
        //defining a series
       
        series.setName("Soile Temerature");
        //populating the series with data
        
        series.getData().add(new XYChart.Data(1, x[0]));
        series.getData().add(new XYChart.Data(2, x[1]));
        series.getData().add(new XYChart.Data(3, x[2]));
        series.getData().add(new XYChart.Data(4, 24));
        series.getData().add(new XYChart.Data(5, 34));
        series.getData().add(new XYChart.Data(6, 36));
        series.getData().add(new XYChart.Data(7, 22));
        series.getData().add(new XYChart.Data(8, 45));
        series.getData().add(new XYChart.Data(9, 43));
        series.getData().add(new XYChart.Data(10, 17));
        series.getData().add(new XYChart.Data(11, 29));
        series.getData().add(new XYChart.Data(12, 25));
        
        scene  = new Scene(lineChart,800,600);
       lineChart.getData().add(series);
       
        primarystage.setScene(scene);
        primarystage.show();
        
    }
    
 public  void setXYvalues(int[] x, int[] y)
 {
     for (int i=0; i<x.length;i++)
     {
         series.getData().add(new XYChart.Data(x[i], y[i]));
        }
        series.getData().add(new XYChart.Data(1, 23));
        series.getData().add(new XYChart.Data(2, 14));
        series.getData().add(new XYChart.Data(3, 15));
        series.getData().add(new XYChart.Data(4, 24));
        series.getData().add(new XYChart.Data(5, 34));
        series.getData().add(new XYChart.Data(6, 36));
        series.getData().add(new XYChart.Data(7, 22));
        series.getData().add(new XYChart.Data(8, 45));
        series.getData().add(new XYChart.Data(9, 43));
        series.getData().add(new XYChart.Data(10, 17));
        series.getData().add(new XYChart.Data(11, 29));
        series.getData().add(new XYChart.Data(12, 25));
       //lineChart=new LineChart<Number,Number>(xAxis,yAxis);
     lineChart.getData().add(series);
      lineChart.setTitle("Hamza");
      //start(primarystage);
      primarystage.show();
     /*scene  = new Scene(lineChart,800,600);
       lineChart.getData().add(series);
       
        primarystage.setScene(scene);
        primarystage.show();
     */
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    /*
    public static void run() {
        launch();
    }
    */
}