import org.bluej.WeatherStation.Sensors.TemperatureSensor;
import org.bluej.WeatherStation.Units.Temperature;
import org.bluej.WeatherStation.Implementations.Factory;
import org.bluej.WeatherStation.Drivers.DS18B20;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
//ddd
/**
 * Write a description of class SoilTemperature here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SoilTemperature
{   /**
     * How often to check the Temperature.
     */
    public static final int INTERVAL_MILLIS = 10000;

    /**
     * How many times to check the Temperature when asked for a reading.
     */
    public static final int ITERATIONS = 4; // instance variables - replace the example below with your own
    // instance variables - replace the example below with your own
    private DS18B20 soil_probe_temp;

    /**
     * Constructor for objects of class SoilTemperature
     */
    public SoilTemperature()
    {
        // initialise instance variables
        try{
            DS18B20 soil_probe_temp=new DS18B20();
        }
        catch (Exception e){
            System.out.println("Driver DS18B20 not found");

        }

    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public void plot()
    {
        // put your code here
        int[] x=new int[3];
        x[0]=80;
        x[1]=20;
        x[2]=30;
        int[] y=new int[3];
        y[0]=10;
        y[1]=20;
        y[2]=30;
        Platform.setImplicitExit(false);
        //  SwingUtilities.invokeLater(() -> {
        new JFXPanel(); // initializes JavaFX environment

        // });

        // Platform.runLater(() -> {

        LineChartSample graph=new LineChartSample();
        graph.x=x;
        graph.y=y;
        graph.launch1();

    }
    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public double[]  readTemperature(int y)
    {
        double[] temperatures = new double[ITERATIONS];
try{
        for (int i = 0; i < ITERATIONS; i++) {
            Thread.sleep(INTERVAL_MILLIS);
            temperatures[i] = soil_probe_temp.read();
            System.out.println(temperatures[i]);
        }
    }
         catch (Exception e){
            System.out.println("Driver DS18B20 not found");

        }  
           return temperatures; 
            
    }
}
