import org.bluej.WeatherStation.Units.WindDirection;
import org.bluej.WeatherStation.Implementations.Factory;
import org.bluej.WeatherStation.Implementations.Platform;
import org.bluej.WeatherStation.Sensors.WindVaneSensor;
import org.bluej.WeatherStation.Drivers.DS18B20;
/**
 * Write a description of class WindVane here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class WindVane
{
    /**
     * How often to check the wind direction.
     */
    public static final int INTERVAL_MILLIS = 10000;

    /**
     * How many times to check the wind van when asked for a reading.
     */
    public static final int ITERATIONS = 4;
    
    // instance variables - replace the example below with your own
     private WindVaneSensor sensor;

    /**
     * Constructor for objects of class WindVane
     */
    public WindVane()
    {
        sensor = new Factory(Platform.MOCK).getWindVaneSensor();
    }

    /**
     * Runs for a while, asking the wind sensor for readings every few seconds, and returns an array of wind speeds.
     * @throws InterruptedException, Required so that "Thread.sleep" is handled correctly.
     */
    public WindDirection[] getReadings() throws InterruptedException {

        WindDirection[] windDirections = new WindDirection[ITERATIONS];

        for (int i = 0; i < ITERATIONS; i++) {
            Thread.sleep(INTERVAL_MILLIS);
            windDirections[i] = sensor.getWindDirection();
            System.out.println(windDirections[i].getDegrees());
        }

        return windDirections;
    }
    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public int sampleMethod(int y)
    {
        // put your code here
        return 0;
    }
}
